
package proyecto1.pkg1;

public class PM 
{
    private String identificador;
    private String nombreP;
    private Integer fechaInicio;
    private Integer fechaCierre;
    
    public void crear()
    {
        
    }

    public void editar()
    {
        
    }
    
    public String listar()
    {
        return "";
    }
    
    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public Integer getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Integer fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Integer getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Integer fechaCierre) {
        this.fechaCierre = fechaCierre;
    }
}
