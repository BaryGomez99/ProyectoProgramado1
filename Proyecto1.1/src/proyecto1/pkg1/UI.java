
package proyecto1.pkg1;

import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import java.io.IOException;

public class UI 
{
    private static Console console = System.console();
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    
    public static String[] input(String name, String[] msgs) // varias entradas
    {
        String[] inputs=new String[msgs.length];
        outLn("----------"+name+"---------");
        for(int i=0; i<msgs.length;i++)
        {
            inputs[i]=input(msgs[i]);
        }
        return inputs;
    }
    
    public static String input(String msg) // una sola entrada
    {
       Boolean opt=null;
       String dato;
       String yn;
       outLn(msg); 
       dato=readLine();
       do{
          outLn("Esta seguro?[y/n]");
          yn=readLine();
          if(yn.startsWith("y"))opt=true;
          if(yn.startsWith("n"))opt=false;
       }while(opt==null);
       if(opt) return dato;
       return null;
    }
    
    private static String readLine()
    {
        if(console!=null)
        {
            return console.readLine();
        }else{
            try{
                return reader.readLine();
            }
            catch(IOException ex){
                throw new RuntimeException(ex);
            }
        }    
    }
    
    public static void outLn(String salida)
    {
        System.out.println(salida);
    }
    
    public static int menu(String name, String [] opciones)
    {
        int opt=-1;
        outLn("-------------"+name+"-------------");
        outLn("Seleccione una opcion.\n");
        for(int i=0;i<opciones.length; i++)
        {
            outLn((i+1)+"---------"+opciones[i]);
        }
        do{
            opt=Integer.parseInt(readLine().trim());
            if(opt-1>=opciones.length || opt<=0)
            {
                outLn("Ingrese una opcion entre 1 y "+opciones.length);
                opt=-1;
            }
        }while(opt==-1);
        return opt-1;
    }
}
