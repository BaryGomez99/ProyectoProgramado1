
package proyecto1.pkg1;

public class AdTareas 
{
    private Integer Id;
    private String descripcion;
    private Colaborador colaborador;
    private Sprints sprint;
    private Integer dateBeginP;
    private Integer dateEndP;
    private String recursos;
    
    public void AgregarT()
    {
        
    }
    
    public void ModificarT()
    {
        
    }
    
    public void EliminarT()
    {
        
    }
    
    public String DesplegarT()
    {
        return "";
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public Sprints getSprint() {
        return sprint;
    }

    public void setSprint(Sprints sprint) {
        this.sprint = sprint;
    }

    public Integer getDateBeginP() {
        return dateBeginP;
    }

    public void setDateBeginP(Integer dateBeginP) {
        this.dateBeginP = dateBeginP;
    }

    public Integer getDateEndP() {
        return dateEndP;
    }

    public void setDateEndP(Integer dateEndP) {
        this.dateEndP = dateEndP;
    }

    public String getRecursos() {
        return recursos;
    }

    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }
}
